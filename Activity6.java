package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

//Make sure that the “Activities” menu item exists and is clickable

public class Activity6
{
	WebDriver driver;

	@BeforeClass
	public void beforeClass() 
	{
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/crm");
		System.out.println("Browser has been launched");
	}

	@Test
	public void testcase6() 
	{
		//Logging into website
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		System.out.println("Title of the page : " + driver.getTitle());

		//Locate the navigation menu and Ensure that the “Activities” menu item exists and is clickable
		 WebDriverWait wait = new WebDriverWait(driver,40);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li"))); 
		 List<WebElement> menu = driver.findElements(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li"));
	     
		 for(WebElement result1 : menu)
	     {
	    	 String result2 = result1.getText(); 
	    	 System.out.println("The menu items are : " + result2);
	    	 if(result2.contains("ACTIVITIES"))
	    	 {
	    		System.out.println("Activities Menu is present");
	    	  }
	     }
	       driver.findElement(By.id("grouptab_3")).click();
	}

	@AfterClass
	public void afterClass()
	{
		driver.close();
		System.out.println("Closing the Browser");
	}

}

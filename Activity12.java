package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//To schedule a meeting and include at least 3 invitees.

public class Activity12 
{
	WebDriver driver;
  
  @BeforeClass
  public void beforeClass()
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase12()
  {
	     //Logging into website
		 driver.findElement(By.id("user_name")).sendKeys("admin");
		 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		 driver.findElement(By.id("bigbutton")).click();
		 System.out.println("Title of the page : " + driver.getTitle());
		 
		 //Navigate to Activities -> Meetings -> Schedule a Meeting
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 driver.findElement(By.id("grouptab_3")).click();
		 WebDriverWait wait3 = new WebDriverWait(driver,20);
		 wait3.until(ExpectedConditions.visibilityOfElementLocated(By.id("moduleTab_9_Meetings")));
		 driver.findElement(By.id("moduleTab_9_Meetings")).click();
		 driver.findElement(By.xpath("//div[contains(text(),'Schedule Meeting')]")).click();
		 
		 //Enter the details of the meeting
		 WebDriverWait wait1 = new WebDriverWait(driver,20);
		 wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
		 driver.findElement(By.id("name")).sendKeys("Discussion1");
		 driver.findElement(By.id("description")).sendKeys("Discussion about the requirements");
		 
		 //Search for members and add them to the meeting and save
         driver.findElement(By.id("search_first_name")).sendKeys("a");
         driver.findElement(By.id("invitees_search")).click();
         driver.findElement(By.id("invitees_add_1")).click();
         WebDriverWait wait2 = new WebDriverWait(driver,40);
         wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("search_first_name")));
         WebElement search2 = driver.findElement(By.id("search_first_name"));
         search2.clear();
         search2.sendKeys("c");
         driver.findElement(By.id("invitees_search")).click();
         WebDriverWait wait4 = new WebDriverWait(driver,20);
         wait4.until(ExpectedConditions.elementToBeClickable(By.id("invitees_add_7")));
         driver.findElement(By.id("invitees_add_7")).click();
         driver.findElement(By.id("SAVE_HEADER")).click();
         
         //Navigate to View Meetings page and confirm creation of the meeting
         driver.findElement(By.partialLinkText("View Meetings")).click();
         WebDriverWait wait5 =  new WebDriverWait(driver,20);
         wait5.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='list view table-responsive']/tbody/tr/td[4]")));
         WebElement meeting = driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody/tr/td[4]"));
         String result1 = meeting.getText();
         Assert.assertEquals(result1, "Discussion1"); 
         Reporter.log("Meeting has been scheduled successfully", true);
        }

  @AfterClass
  public void afterClass() 
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

//Reading a popup that contains additional information about the account/lead.

public class Activity7
{
  WebDriver driver;
    	
  @BeforeClass
  public void beforeClass() 
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }

  @Test
  public void testcase7()
  {
	  //Logging into website
	  driver.findElement(By.id("user_name")).sendKeys("admin");
	  driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
	  driver.findElement(By.id("bigbutton")).click();
	  System.out.println("Title of the page : " + driver.getTitle());
	  
	  //Clicking on sales menu
	  driver.findElement(By.id("grouptab_0")).click();
	  
	  //Clicking on Leads option
	   driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	   driver.findElement(By.id("moduleTab_9_Leads")).click();
	  
	  //Clicking on Additional Information icon 
	   WebDriverWait wait1 = new WebDriverWait(driver,20);
	   wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".list")));
	   driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody/tr[1]/td[10]")).click();
	  
	    //Printing the Phone Number in the console
	    String phonenumber = driver.findElement(By.xpath("//div[@id='ui-id-5']/h2/following::span[1]")).getText();
        System.out.println("The Phone number is : " + phonenumber);		 
  }
  
  @AfterClass
  public void afterClass()
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

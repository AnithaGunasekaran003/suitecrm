package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

//Get the color of the navigation menu

public class Activity5 
{
  WebDriver driver;
	
  @BeforeClass
  public void beforeClass()
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase5()
  {
	  //Logging into website
	  driver.findElement(By.id("user_name")).sendKeys("admin");
	  driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
	  driver.findElement(By.id("bigbutton")).click();
	  System.out.println("Title of the page : " + driver.getTitle());
	  
	  //Printing the color of the navigation menu
	  WebElement color1 = driver.findElement(By.id("toolbar"));
	  System.out.println("The color of the Navigation Menu : " + color1.getCssValue("color"));
	  System.out.println("TestCase5 has been completed successfully");
  }

  @AfterClass
  public void afterClass()
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

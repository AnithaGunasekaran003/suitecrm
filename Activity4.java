package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

//Open the site and login with the credentials provided

public class Activity4 
{
  WebDriver driver;
  WebDriverWait wait;
    
  @BeforeClass
  public void beforeClass()
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase4() 
  {
	  //Logging into website
	  WebElement username = driver.findElement(By.id("user_name"));
	  WebElement password = driver.findElement(By.id("username_password"));
	  
	  username.sendKeys("admin");
	  password.sendKeys("pa$$w0rd");
	  driver.findElement(By.id("bigbutton")).click();
	  System.out.println("Title of the page : " + driver.getTitle());
	  
	  //verifying the display of home page
	  wait = new WebDriverWait(driver,40);
	  boolean result = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("desktop-toolbar"))).isDisplayed();
	  System.out.println("The Home Page is opened : " + result);
	  System.out.println("Test Case4 completed successfully");
  }

  @AfterClass
  public void afterClass() 
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

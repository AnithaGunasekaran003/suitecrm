package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

//To Read the title of the website and verify the text

public class Activity1
{
	WebDriver driver;
	  
  @BeforeClass
  public void beforeClass() 
  {
	  driver = new FirefoxDriver();
	  driver.get("http://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
   }
    
  @Test
  public void testcase1() 
  {
	  //verifying the title of the text
	  String title = driver.getTitle();
	  System.out.println("Title of the page is : " + title);
	  Assert.assertEquals(title, "SuiteCRM", "Title test failed"); 
	  System.out.println("Test Case1 has been completed successfully");
  }

  @AfterClass
  public void afterClass() 
  {
	  if(driver.getTitle().equals("SuiteCRM"))
	  {
       driver.close();
       System.out.println("Closing the Browser");
	  }
	  
  }

}

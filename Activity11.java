package projectactivity1;

//Create multiple leads by importing a CSV file
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Activity11
{
	WebDriver driver;
	
  @BeforeClass
  public void beforeClass() 
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase11() 
  {
	     //Logging into website
		 driver.findElement(By.id("user_name")).sendKeys("admin");
		 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		 driver.findElement(By.id("bigbutton")).click();
		 System.out.println("Title of the page : " + driver.getTitle());
		 
		  //Navigate to Sales -> Leads -> Import Leads
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  driver.findElement(By.id("grouptab_0")).click();
		  driver.findElement(By.id("moduleTab_9_Leads")).click();
		  driver.findElement(By.xpath("//div[contains(text(),'Import Leads')]")).click();
		  
		  //Import the file and submit
		  File file1 = new File("src/resources/ImportLeads.csv");
		  WebElement upload = driver.findElement(By.id("userfile"));
		  upload.sendKeys(file1.getAbsolutePath());
		  
		  driver.findElement(By.id("gonext")).click();
		  driver.findElement(By.id("gonext")).click();
		  driver.findElement(By.id("gonext")).click();
		  driver.findElement(By.id("importnow")).click();
		  driver.findElement(By.id("finished")).click();
		  
         //Navigate to the View Leads page to see results
		 driver.findElement(By.linkText("View Leads")).click();
		 WebDriverWait wait = new WebDriverWait(driver,30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='list view table-responsive']/tbody/tr/td[3]")));
		 List<WebElement> options = driver.findElements(By.xpath("//table[@class='list view table-responsive']/tbody/tr/td[3]"));
		  
		 System.out.println("The names are :");
		 for(WebElement result1 : options)
		 {
		  String result2 = result1.getText();
		  System.out.println(result2);
		  if(result2.contains("Avery"))
		  {
		  System.out.println("File Import has been completed successfully");
		  }  
		 }
   }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

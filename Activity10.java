package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

//Open the homepage and count the number of the dashlets on the page

public class Activity10
{
	WebDriver driver;
	
  @BeforeClass
  public void beforeClass()
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase10()
  {
	     //Logging into website
		 driver.findElement(By.id("user_name")).sendKeys("admin");
		 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		 driver.findElement(By.id("bigbutton")).click();
		 System.out.println("Title of the page : " + driver.getTitle());
		 
		 //To count the number of dashlets in the Home page
		 WebDriverWait wait1 = new WebDriverWait(driver,40);
		 wait1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("td[class*='dashlet-title']")));
		 List<WebElement> dashlets = driver.findElements(By.cssSelector("td[class*='dashlet-title']"));
		 System.out.println("The total number of dashlets in the HOME PAGE are : " + dashlets.size());
		 
		 //Print the number and title of each Dashlet into the console
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 List<WebElement> names = driver.findElements(By.xpath("//table[@class='formHeader h3Row']/tbody/tr[1]/td[1]/h3"));
		 System.out.println("The Number and Title of the Dashlet are: ");
		 
		 for(int i=0;i<names.size();i++)
		 {
			 WebElement dashlet = names.get(i);
			 System.out.println(i +  "\t" + dashlet.getText());
		 }
  }

  @AfterClass
  public void afterClass() 
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

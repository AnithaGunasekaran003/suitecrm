package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

//Print the url of the header image to the console

public class Activity2 
{
 
  WebDriver driver;
	
  @BeforeClass
  public void beforeClass() 
  {
	  driver = new FirefoxDriver();
	  driver.get("http://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase2() 
  {
	  //Printing the URL of the header image
	  String headerimageurl = driver.findElement(By.cssSelector("a[href*='suitecrm']")).getAttribute("href");
	  System.out.println("URL of the header image : " + headerimageurl);
	  System.out.println("TestCase2 has been completed succesfully");
  }

  @AfterClass
  public void afterClass()
  {
	driver.close();  
	System.out.println("Closing the Browser");
  }

}

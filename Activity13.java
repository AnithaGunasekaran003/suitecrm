package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

//To use an external Excel to add products

public class Activity13 
{
	WebDriver driver;
			
  @BeforeClass
  public void beforeClass() 
  {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase13() 
  {
	     //Logging into website
		 driver.findElement(By.id("user_name")).sendKeys("admin");
		 driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		 driver.findElement(By.id("bigbutton")).click();
		 System.out.println("Title of the page : " + driver.getTitle());
		 
		 //Navigate to All -> Products-> Create Product
		 driver.findElement(By.id("grouptab_5")).click();
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 WebElement dropdown = driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[7]/span[2]/ul/li[25]/a"));
		 JavascriptExecutor js = (JavascriptExecutor)driver;
		 js.executeScript("arguments[0].scrollIntoView(true);", dropdown);
		 driver.findElement(By.linkText("Products")).click();
		 driver.findElement(By.cssSelector(".actionmenulink")).click();
			
		//Retrieve information about the product from an external Excel file
		String filepath = "src/resources/Sample.xlsx";
		Activity13 obj1 = new Activity13();
		List<List<String>> data = obj1.readExcel(filepath);
        List<String> row;
        
        WebDriverWait wait1 = new WebDriverWait(driver,20);
        
        //Enter the details of the product and save
        for(int i=1;i<=5;i++)
        {
        row = data.get(i);
        
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
        WebElement productname = driver.findElement(By.id("name"));
        WebElement price = driver.findElement(By.id("price"));
        
        productname.sendKeys(row.get(0));
        price.sendKeys(row.get(1));        
        driver.findElement(By.id("SAVE")).click();
        driver.navigate().back();
        }
        
        //Go to the �View Products� page to see all products listed.
        driver.findElement(By.partialLinkText("View Products")).click();
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='list view table-responsive']/tfoot/tr/td/table/tbody/tr/td[3]")));
        WebElement productlist = driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody/tr[1]/td[3]"));
        String product = productlist.getText();
        Assert.assertEquals(product,"Iphone6");
        System.out.println("The products are created successfully");
        
  }  
  
  public List<List<String>> readExcel(String filepath) 
  {
      List<List<String>> data = new ArrayList<List<String>>();
      try 
      {
          FileInputStream file = new FileInputStream(filepath);

          //Create Workbook instance holding reference to Excel file
          XSSFWorkbook workbook = new XSSFWorkbook(file);

          //Get first sheet from the workbook
          XSSFSheet sheet = workbook.getSheetAt(0);

          //Iterate through each rows one by one
          Iterator<Row> rowIterator = sheet.iterator();
          while(rowIterator.hasNext()) 
          {
              //Temp variable
              List<String> rowData = new ArrayList<String>();
              Row row = rowIterator.next();
              
              //For each row, iterate through all the columns
              Iterator<Cell> cellIterator = row.cellIterator();

              while (cellIterator.hasNext())
              {
                  Cell cell = cellIterator.next();
                  rowData.add(cell.toString());  
              }
              //Store row data in List
              data.add(rowData);
          }
          file.close();
          workbook.close();
      }
      catch (Exception e) {
          e.printStackTrace();
      }
      return data;
  }

  @AfterClass
  public void afterClass()
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}

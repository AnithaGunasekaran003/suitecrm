package projectactivity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

//Print the first copyright text in the footer to the console

public class Activity3 
{
  WebDriver driver;
  
  @BeforeClass
  public void beforeClass()
  {
	  driver = new FirefoxDriver();
	  driver.get("http://alchemy.hguy.co/crm");
	  System.out.println("Browser has been launched");
  }
  
  @Test
  public void testcase3() 
  {
	  //Printing the first copyright text
	  String copyrighttext1 = driver.findElement(By.id("admin_options")).getText();
	  System.out.println("The first Copyright text in the footer is : " + copyrighttext1);
	  System.out.println("TestCase3 has been completed successfully");
  }
  
  @AfterClass
  public void afterClass()
  {
	  driver.close();
	  System.out.println("Closing the Browser");
  }

}
